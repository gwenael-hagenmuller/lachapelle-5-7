import os
this_folder_path = os.path.dirname(os.path.realpath(__file__))
print("Executing '" + this_folder_path + "'...")

kmz_folder_path = os.path.join(this_folder_path, "..", "kmz")

print("Deleting '" + kmz_folder_path + "'...")

import shutil
shutil.rmtree(kmz_folder_path)

print("Retrieving map id...")

mid_file_path = os.path.join(this_folder_path, "..", "mid.txt")

with open(mid_file_path, "r") as mid_file:
    mid = mid_file.read()

kmz_url = "http://www.google.com/maps/d/u/0/kml?mid=" + mid

print("Downloading kmz file from " + kmz_url + "...")

import urllib2
response = urllib2.urlopen(kmz_url)
kmz = response.read()

tmp_folder_path = os.path.join(this_folder_path, "tmp")
kmz_file_path = os.path.join(tmp_folder_path, mid + ".kmz")

if not os.path.isdir(tmp_folder_path):
    os.makedirs(tmp_folder_path)

with open(kmz_file_path, "wb") as kmz_file:
    kmz_file.write(kmz)

print("Unzipping kmz file to '" + kmz_folder_path + "'...")

import zipfile, sys
with zipfile.ZipFile(kmz_file_path, "r") as kmz_file:
    for m in kmz_file.infolist():
        data = kmz_file.read(m) # extract zipped data into memory
        # convert unicode file path to utf8
        disk_file_name = m.filename.encode('utf8')
        disk_file_name = os.path.join(kmz_folder_path, disk_file_name)
        dir_name = os.path.dirname(disk_file_name)
        try:
            os.makedirs(dir_name)
        except OSError as e:
            if e.errno == os.errno.EEXIST:
                pass
            else:
                raise
        except Exception as e:
            raise

        with open(disk_file_name, "wb") as fd:
            fd.write(data)

kml_file_path = os.path.join(kmz_folder_path, "doc.kml")
xlst_file_path = os.path.join(this_folder_path, "doc.kml.xlst")

print("Transforming kml file '" + kml_file_path + "...")

import lxml.etree as ET #pip install lxml (Python >= 2.7.15)

dom = ET.parse(kml_file_path)
xslt = ET.parse(xlst_file_path)
transform = ET.XSLT(xslt)
newdom = transform(dom)

with open(kml_file_path, "wb") as kml_file: #opened in binary to avoid eol conversion
    #kml_file.write(ET.tostring(newdom, pretty_print=True))
    kml_file.write(str(newdom))
